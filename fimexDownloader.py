from netCDF4 import Dataset as NetCDFFile
from datetime import datetime, timedelta
import subprocess
import argparse
import time
import os
import sys

class FimexDownloader:

	def __init__(self, lat, lng, separator = ";", excludeCols = [], translateCols = {}):
		""" Initialize a FimexDownloader. Must provide latitude and longitude values for the location to download, and
		can additionally provide a CSV separator for the output file, the columns to exclude, and the column headers to replace.
		"""

		self.lat = lat
		self.lng = lng
		
		self.separator = separator
		
		self.excludeCols = excludeCols
		self.translateCols = translateCols

	def getQueryTimes(self, fromDate, toDate):
		""" Returns all query times between the fromDate and the toDate.
		There are four query times for each day: 00:00, 06:00, 12:00, 18:00.
		"""

		delta = toDate - fromDate

		dates = []
		for day in range(delta.days + 1):
			for hour in [0, 6, 12, 18]:
				dates.append(fromDate + timedelta(day, hours=hour))
		return dates

	def getFimexConfig(self, fileName, queryTime, variables):
		""" Generates a Fimex config file. If you need to use this class for anything,
		this is probably the most important function to modify.
		"""

		config = """[input]
	file=http://thredds.met.no/thredds/dodsC/meps25epsarchive/{year}/{month}/{day}/{filename}_{year}{month}{day}T{hour}Z.nc
	type=nc4

	[output]
	file={datafile}
	type=nc4

	[extract]"""

		config = config.replace("{year}", queryTime.strftime("%Y"))
		config = config.replace("{month}", queryTime.strftime("%m"))
		config = config.replace("{day}", queryTime.strftime("%d"))
		config = config.replace("{hour}", queryTime.strftime("%H"))

		datafile = "data-" + queryTime.strftime("%Y-%m-%d-%H") + ".nc"
		config = config.replace("{datafile}", datafile)

		config = config.replace("{filename}", fileName)

		for var in variables:
			config += "\nselectVariables=" + var

		config += """

	[interpolate]
	latitudeValues={lat}
	longitudeValues={lng}

	[timeInterpolate]
	timeSpec="""

		config = config.replace("{lat}", str(self.lat));
		config = config.replace("{lng}", str(self.lng));

		times = ""
		for minutes in range(0, 360, 10):
			times += ("" if times == "" else ",") + (queryTime + timedelta(minutes = minutes)).isoformat() + "Z"
		config += times

		return config, datafile

	def getFimexConfigRegular(self, queryTime):
		""" Convenience function for generating a Fimex config for the pp file.
		"""

		variables = ["surface_air_pressure", "air_pressure_at_sea_level", "relative_humidity_2m", "precipitation_amount", "fog_area_fraction", "wind_speed_of_gust", "x_wind_10m", "y_wind_10m", "air_temperature_2m"]
		return self.getFimexConfig("meps_mbr0_pp_2_5km", queryTime, variables)

	def getFimexConfigSurface(self, queryTime):
		""" Convenience function for generating a Fimex config for the sfx file.
		"""

		variables = ["TS"]
		return self.getFimexConfig("meps_mbr0_sfx_2_5km", queryTime, variables)

	def readNc4(self, nc):
		""" Reads all data from the given NC file, and returns it as a header string, 
		and a dictionary with the different times as keys, and the data as values.
		"""

		# Fetch all column names, exclude the ones that should be excluded, and replace
		# the headers if they are in translateCols.
		cols = [x for x in nc.variables if x not in self.excludeCols]
		cols = [self.translateCols[x] if (x in self.translateCols) else x for x in cols]

		# Generate the CSV header string
		header = self.separator.join(["time"] + cols)

		# Enumerate each time in the NC file, and add the data for each column to the data dict.
		data = {}
		for index, time in enumerate(nc.variables["time"]):
			line = []
			for var in nc.variables:

				# If the column should be excluded, skip it.
				if var in self.excludeCols: continue

				values = nc.variables[var]

				# If this column doesn't have a value for this time, use the last value instead.
				if index >= len(values): line.append(self.toString(values[len(values) - 1]))

				# Otherwise, extract the value for this time.
				else: line.append(self.toString(values[index]))

			# Store the CSV data in the data dict.
			data[self.toString(time)] = self.separator.join(line)

		return header, data

	def toString(self, data):
		""" Convenience function for stringifying a NC value
		"""

		return str(data).replace("[", "").replace("]", "")

	def getDataFromFimex(self, time, configType):
		""" Generate a Fimex configuration file, launch Fimex using this config file,
		read the data Fimex downloaded, and delete the temporary files that were 
		created in the process.
		"""

		# Prepare configuration
		config, datafile = self.getFimexConfigRegular(time) if configType == "regular" else self.getFimexConfigSurface(time)
		configfile = "fimex-config-" + time.isoformat().replace(":", "-") + ".cfg"

		# Write configuration to a file
		with open(configfile, "w") as file:
			file.write(config)

		# Run Fimex with this config file
		subprocess.call(["fimex-0.66", "-c", configfile])
	
		# Read data from the NC file that was created by Fimex
		nc = NetCDFFile(datafile)
		header, data = self.readNc4(nc)

		# Delete the temporary files
		os.remove(configfile)
		os.remove(datafile)

		return header, data


	def download(self, filePrefix, fromDate, toDate):
		""" Will download data from Fimex for the given time period, and store
		it in CSV files.
		"""

		# Get all the query times we need to fetch data for
		queryTimes = self.getQueryTimes(datetime(2017, 11, 01), datetime(2018, 05, 01))

		# Internal function for multi threading
		def downloadDataForTime(queryTime):

			try:
	
				# Generate the output filename
				outputfile = filePrefix + queryTime.isoformat().replace(":", "-") + ".csv"

				# If it already exists, skip the download.
				if os.path.isfile(outputfile):
					print "    > Skipped " + queryTime.isoformat() + ", already downloaded."
					return outputfile

				start = time.time()

				# Download the main data (pp)
				header, data = self.getDataFromFimex(queryTime, "regular")

				# Download the surface data (sfx)
				_, surfaceData = self.getDataFromFimex(queryTime, "surface")

				# Merge the data from the two downloads
				csv = header + ";surfaceTemperature"
				for key in sorted(data.iterkeys()):
					csv += "\n" + key + self.separator + data[key] + self.separator + surfaceData[key]

				# Write the merged data to a CSV file
				with open(outputfile, "w") as file:
					file.write(csv)

				print "    > Downloaded " + queryTime.isoformat() + " in " + str(time.strftime("%H:%M:%S", time.gmtime(time.time() - start)))

			except:
				print "    > FAILED to download " + queryTime.isoformat() + ": " + str(sys.exc_info()[0])

			return outputfile

		return map(downloadDataForTime, queryTimes)

	def merge(self, files, outputfile):
		""" Merges all downloaded CSV files into one large CSV file.
		"""

		isFirstFile = True
		with open(outputfile, "w") as outfile:
			for fname in files:
				if not os.path.isfile(fname):
					continue
				with open(fname) as infile:
					isFirstLine = True
					for line in infile:
						if isFirstFile or not isFirstLine:
							outfile.write(line)
						isFirstLine = False
				outfile.write("\n")
				isFirstFile = False
		

if __name__ == "__main__":

	# Utilize the FimexDownloader class to download data for Fokstugu at Dovre.

	# Columns we don't want in the final CSV file, and column headers we want to replace.
	excludeCols = ["time", "x", "y", "latitude", "longitude", "height_above_msl", "height0", "height1"]
	translateCols = {
		"TS": "surfaceTemperature"
	}

	# Initialize the FimexDownloader class with the coordinates for Fokstugu, and the column data.
	fimex = FimexDownloader(62.111881, 9.284969, excludeCols = excludeCols, translateCols = translateCols)
	
	# Date range to download
	fromDate = datetime(2017, 11, 01)
	toDate = datetime(2018, 05, 01)
	
	# Do the actual download
	files = fimex.download("fokstugu_", fromDate, toDate)

	# Merge all the downloaded CSV files into one large CSV file.
	fimex.merge(files, "fokstugu_" + fromDate.isoformat().replace(":", "-") + "-" + toDate.isoformat().replace(":", "-") + ".csv")
