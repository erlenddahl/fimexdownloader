# FimexDownloader

This is a simple Python 2.7 script that utilizes the Norwegian Meteorological Institute's ["Fimex" tool](https://wiki.met.no/fimex/start) to download weather forecasts from their [Thredds servers.](https://thredds.met.no/thredds/catalog.html) 

We needed to download weather forecasts to train an AI model. The data we needed existed in two different files on Thredds. This script downloads both files four times (00:00, 06:00, 12:00, and 18:00) for each required date, and merges the data from the two NC files into a single CSV file per time step. Finally, all the CSV files are merged together to one large CSV file that contains all the data we need for the entire period.

## Usage

First, you need to install Fimex on your system. This is easiest done by running a virtual Ubuntu PC, adding the [Met Norway Fimex PPA](https://launchpad.net/~met-norway/+archive/ubuntu/fimex/+packages), and then installing Fimex. This tool was developed and tested with fimex-0.66 on an Ubuntu 16.04, but should work with any version.

Import the class, or modify the ________main________ code at the bottom of the file to use it.

If you need to download data from other Thredds files than the ones we used in this case, you need to modify the getFimexConfig functions.